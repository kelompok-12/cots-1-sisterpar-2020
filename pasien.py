# import library socket
import socket

# mendefinisikan tujuan IP server(dokter)
ip = 'localhost'

#mendefinisikan port dari server(dokter)
port = 12345

#mendefinikan ukuran buffer untuk mengirimkan pesan
size = 1024

#mendefinisikan notification yang akan di kirim ke server(dokter), 
#jika kadar gulanya diatas 140(diatas batas normal), maka client(pasien) mengirim notification ke dokter
print("----------------------------------------------------------")
print("------------------INPUT KADAR GULA DARAH------------------\n")
while  True:
	pass
	g = int(input("Kadar Gula Darah : "))
	if (g > 140):
		pesan = "Kadar gula darah pasien "+str(g)+", Pasien Kelebihan kadar gula darah"
		#membuat socket TCP
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		#melakukan koneksi ke server(dokter) dengan parameter IP dan Port yg tadi sudah didefinisikan
		s.connect((ip, port))

		#kirim pesan(notif) ke server(dokter)
		s.send(pesan.encode())

		#menerima saran dari dokter
		terima = s.recv(size).decode()

		#menampilkan saran dari dokter
		print("\n\n----------------------------------------------------------")
		print("|-----------------------SARAN DOKTER-----------------------|\n")
		print(terima)
		print("|----------------------------------------------------------|")

		#tutup koneksi
		s.close()
		break
	else:
		break