# import library socket karena akan menggunakan IPC socket
import socket

# definisikan alamat IP binding  yang akan digunakan
ip = "localhost"

# definisikan port number binding  yang akan digunakan
port = 12345

# definisikan ukuran buffer untuk mengirimkan pesan
size = 1024

# buat socket bertipe TCP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

# lakukan bind
s.bind((ip,port))

# server akan listen menunggu hingga ada koneksi dari client
s.listen(5)
print("socket is listening")

# lakukan loop forever
while 1:
	# menerima koneksi
	c, addr = s.accept()
	#print ('Got connection from', addr)

	# menampilkan koneksi berupa IP dan port client yang terhubung menggunakan print
	pesan = "soket bind ke alamat" + ip +" dan port " + str(port)
	#print(pesan)

	# menerima data berdasarkan ukuran buffer
	var = (c.recv(1024).decode())
	print("pesan diterima")
	print("==========NOTIFIKASI==========")
	print(var)
	

	# mendefinisikan saran yang akan dikirimkan ke client(pasien), jika terdapat notifikasi dari client(pasien) 
	saran = "saran dokter	: "+" \n "+"Gula Darah anda berlebih/melebihi batas normal. Kami me-"+" \n "+"nyarankan untuk mengkonsumsi berbagai nutrisi untuk me-"+" \n "+"ngurangi kadar gula darah anda dan lakukan olahraga yang"+" \n "+"teratur."

	# mengirimkan saran ke pasien
	c.send(saran.encode())

# tutup koneksi
c.close()
