# Sistem Pengontrol Gula Darah

## a. Deskripsi Aplikasi
Ini adalah program untuk pengontrol gula darah sederhana antara dokter dan
pasien. Program ini menggunakan metode Interprocess Communication(IPC).
Dalam IPC ini terdapat client dan server dimana clientnya adalah pasien dan
servernya adalah dokter. Langkah Pertama user(pasien) menginputkan kadar gula
darahnya ke aplikasi pasien(pasien.py). Setelah itu aplikasi pasien(pasien.py)
mengecek kondisi, jika kadar gula darah melebihi 140(normal), maka aplikasi
pasien(pasien.py) mengirimkan notification ke aplikasi dokter(dokter.py).
Kemudian aplikasi dokter(dokter.py) menampilkan notification dari pasien.
Kemudian dokter memberikan saran ke pasien dan di kirimkan ke aplikasi pasien
(pasien.py). Aplikasi pasien menampilkan saran yang diterima dari dokter.

## b. Tugas Anggota
Yudhistira Al Alim(Developer+Maintainer)<br/>
Zakiya Ainur Rohman (Developer+Maintainer)

## c. Prosedur Instalasi
1. Buka dua cmd atau terminal
2. Jalankan dokter.py(server) pada salah satu cmd atau terminal
3. Saat server berjalan, jalankan juga pasien.py(client) pada cmd atau terminal lainnya
4. Pada saat client berjalan, akan menampilkan input kadar gula darah. Masukkan kadar gula darah yang mau di cek
5. Jika kadar gula darahnya melebihi batas normal(140), maka pasien.py mengirimkan sebuah notif ke dokter.py
6. Setelah itu, dokter.py menerima dan menampilkan notif yang dikirim dari pasien.py
7. Setelah itu, dokter.py mengirimkan ke pasien.py kembali berupa saran
8. pasien.py akan menerima dan menampilkan saran yang dikirim dari dokter.py
9. Jika kadar gula darahnya yang di input itu normal(kurang dari 140), maka program akan berhenti
